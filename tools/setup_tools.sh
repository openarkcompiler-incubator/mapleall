#!/bin/bash
#
# Copyright (C) [2020] Futurewei Technologies, Inc. All rights reverved.
#
# Licensed under the Mulan Permissive Software License v2.
# You can use this software according to the terms and conditions of the MulanPSL - 2.0.
# You may obtain a copy of MulanPSL - 2.0 at:
#
#   https://opensource.org/licenses/MulanPSL-2.0
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
# FIT FOR A PARTICULAR PURPOSE.
# See the MulanPSL - 2.0 for more details.
#

set -e

TOOLS=`pwd`
echo TOOLS=$TOOLS

if [ ! -f $TOOLS/../bin/ast2mpl ]; then
  cd $TOOLS/../bin/ast2mpl_files
  cat ast2mpl_aa ast2mpl_ab ast2mpl_ac ast2mpl_ad > ast2mpl.gz
  gunzip ast2mpl.gz
  chmod 775 ast2mpl
  mv ast2mpl ..
  echo Merged ast2mpl.
fi

if [ ! -f $TOOLS/ninja/ninja ]; then
  mkdir -p $TOOLS/ninja
  cd $TOOLS/ninja || exit 3
  wget https://github.com/ninja-build/ninja/releases/download/v1.10.0/ninja-linux.zip
  unzip ninja-linux.zip
  echo Downloaded ninja.
fi

if [ ! -f $TOOLS/gn/gn ]; then
  cd $TOOLS
  git clone https://gitee.com/xlnb/gn_binary.git gn
  chmod +x gn/gn
  echo Downloaded gn.
fi

if [ ! -f $TOOLS/open64_prebuilt/README.md ]; then
  cd $TOOLS
  git clone --depth 1 https://gitee.com/open64ark/open64_prebuilt.git -b mapleall
fi
if [ ! -f $TOOLS/open64_prebuilt/x86/riscv64/bin/clangfe ]; then
  cd $TOOLS/open64_prebuilt/x86
  tar zxf open64ark-aarch64.tar.gz
  tar zxf open64ark-riscv.tar.gz
  mv riscv riscv64
  echo Downloaded open64_prebuilt.
fi

if [ ! -f $TOOLS/dwarf/include/dwarf2.h ]; then
  cd $TOOLS
  git clone https://gitee.com/hu-_-wen/dwarf_files.git dwarf
  echo Downloaded dwarf header files.
fi

